-- Answers to Exercise 6 here
# DROP TABLE IF EXISTS videoRentalDatabaseExtra;

CREATE TABLE IF NOT EXISTS videoRentalDatabaseExtra(
  name VARCHAR(20),
  barcode INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  movie_title VARCHAR(100),
  director VARCHAR(100),
  rental_fee  INT,
  CONSTRAINT FOREIGN KEY (name) REFERENCES videoRentalDatabase (name) ON UPDATE CASCADE
);

INSERT INTO videoRentalDatabaseExtra (movie_title, director, rental_fee, name) VALUES
  ('Titanic', 'billy bob', 6, 'Peter Jackson'),
  ('spirited away', 'bob', 4, 'Peter Jackson'),
  ('toystory', 'bob', 2, 'Lorde'),
  ('mahusive_poohpy', 'jackson', 12 , 'Lucy Lawless');


-- [HY000][1215] Cannot add foreign key constraint
