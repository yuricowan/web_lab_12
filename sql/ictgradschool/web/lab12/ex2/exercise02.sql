-- Answers to Exercise 2 here
-- DROP TABLE IF EXISTS useraccounts;
CREATE TABLE useraccounts (
  username VARCHAR(30),
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  email VARCHAR(50)
);

INSERT INTO useraccounts (username, first_name, last_name, email) VALUES
  ('dragonlord17', 'Peter', 'Pe', 'magicslayer12@gmail.com'),
  ('magicKing', 'Pete', 'ter', 'petemcgeet@gmail.com'),
  ('programmer1', 'Bill', 'Peterson', 'bill@microsoft.com');

SELECT * FROM useraccounts;
