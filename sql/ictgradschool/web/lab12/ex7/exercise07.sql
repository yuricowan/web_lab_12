-- Answers to Exercise 7 here
CREATE TABLE comments (
  comment_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  userComment VARCHAR(200),
  article_id INT(100),
  FOREIGN KEY (article_id) REFERENCES website(id)

);

INSERT INTO comments (userComment, article_id) VALUES
  ('very nice', 1),
  ('good article', 2);

