-- Answers to Exercise 5 here
CREATE TABLE useraccounts_ex5 (
  username VARCHAR(30) PRIMARY KEY ,
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  email VARCHAR(50)
);

INSERT INTO useraccounts_ex5 (username, first_name, last_name, email) VALUES
  ('dragonlord17', 'Peter', 'Pe', 'magicslayer12@gmail.com'),
  ('magicKing', 'Pete', 'ter', 'petemcgeet@gmail.com'),
  ('programmer1', 'Bill', 'Peterson', 'bill@microsoft.com'),
  ('programmer1', 'bil', 'bill', 'billy');

SELECT * FROM useraccounts_ex5;

