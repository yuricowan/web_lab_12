-- Answers to Exercise 4 here


CREATE TABLE website (
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  title VARCHAR(100),
  articleContent VARCHAR(7999)
);

INSERT INTO website (title, articleContent) VALUES
  ('Govt reveals immigration changes','The two pay settings which will now be used determine what constitutes a highly skilled job.
   Immigration Minister Michael Woodhouse revealed Queenstown today that the Government plans introduce remuneration thresholds permanent  skilled migrants'),
  ('potato news', 'seven potatoes were murdered');

SELECT * FROM website;