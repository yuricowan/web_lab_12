-- Answers to Exercise 8 here


-- Delete a column
ALTER TABLE videoRentalDatabaseExtra
  DROP COLUMN director;

-- Delete a row
DELETE FROM useraccounts
WHERE username = 'magicKing';

-- delete a table
DROP TABLE comments;

-- change a string
UPDATE videoRentalDatabaseExtra
SET movie_title = 'Titanic'
WHERE movie_title = 'HELLO';

-- change an integer
UPDATE videoRentalDatabaseExtra
SET rental_fee = 4
WHERE rental_fee = 12;

-- change value that is a primary key
UPDATE videoRentalDatabaseExtra
SET barcode = 1
WHERE barcode = 15
